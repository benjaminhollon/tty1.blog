---

title: Guest Authors
subtitle: <code class="language-bash"><span class="token function">whoami</span></code>
description: "I enjoy having guest authors here on `tty1`! If you're interested in writing a guest article, read on.\n\nThe lifecycle of your article will have four main stages: Proposal, Drafting, Editing, and Publication."

---

I enjoy having guest authors here on `tty1`! If you're interested in writing a guest article, read on.

The lifecycle of your article will have four main stages: Proposal, Drafting, Editing, and Publication.

## Proposal

You'll need to write a 150-300 word proposal for your article topic. This both helps you flesh out your concept and helps me get a grasp of what exactly you'll be writing about. Some things you might want to include:

- General topic
- Specific subtopics you'll cover
- Goal of the article

As an example, here's a sample proposal for my [How to Save the World with `tar`](https://tty1.blog/articles/how-to-save-the-world-with-tar/) article:

> `tar` is a notoriously difficult utility to master, but much of that reputation is unfounded. With this article, I would like to break down the concepts behind `tar` and get readers to a level of familiarity where looking up flags in the `man` page is actually helpful rather than daunting.
> 
> I plan to start with a summary of the different flag styles of `tar`, as these are a source of much of the confusion about its usage. Next, I'll walk readers through the most useful flags, including `--verbose`, `--extract`, `--file`, `--list`, `--create`, and `--auto-compress`, explaining both the long flags (which are more straightforward) and the short flags (which are more common). I do not plan to cover the Traditional flags. Finally, I will provide "practice" commands using the short flags to give readers a chance to try and predict their meaning, then check their response against a detailed step-by-step explanation.
> 
> The tone will be lighthearted, as implied by the xkcd-referencing title "How to Save the World with `tar`", to help dispel the aura `tar` has of being unfriendly and difficult to grasp.

Once you've finished your proposal, email it to me at [me@benjaminhollon.com](mailto:me@benjaminhollon.com) ([PGP Public Key](https://tty1.blog/assets/public-keys/benjamin.gpg.asc)) and we'll work out a schedule for the writing, editing, and publication of your article.

I also recommend including the following information in your email for the "About the Author" section below each article:

- A profile picture
- Your full name (or penname)
- A ~50 word biography written in third person (you can use Markdown to, say, link to your personal site)
- A contact email address and PGP public key (note: this will be presented to readers so they can start a conversation about your article)
- A `username@hostname` for yourself

You don't have to include these with your proposal, but I'll need them eventually, so it can help save time to have them ready.

## Drafting

Once I've accepted your proposal, I'll agree with you on a timeline for the article, which will include the amount of time you have to draft the article and a proposed final publication date.

During that time, write away! Try to get a complete article by the end of the drafting period. Once you're done, email it to me!

You'll also need to include a proposed title with your complete draft. I prefer witty titles (Brandon T. Phillips' [Shell, Yes!](https://tty1.blog/articles/shell-yes/) is a great example) but definitely don't require it.

If the timeline I gave you isn't working, don't stress! Life happens. Just send me an email and we'll work out a new timeline.

## Editing

Once I have your complete draft, I'll start editing. This will be a back-and-forth process where I suggest changes and you implement them, then I likely suggest more changes. I won't directly change your wording without checking (except to fix small grammar issues), though I'll sometimes suggest a new wording.

This process may take a while; I like to be rigorous with this process. And to be clear: lots of suggestions from me doesn't mean you're a bad writer! Everyone benefits from feedback and the editorial process, yours truly included.

This could be a long or short process depending on your draft.

## Publication

Finally, after we both agree that your article is ready, I'll publish it to this site. You'll know in advance when that will happen.

On the day of publication and ideally for a couple days afterward, try to be available to address reader questions and feedback—I've caught a number of issues with my own articles post-publication, and we have a very chatty audience! Most will reply via the Fediverse, but some might email you via the address you provide.

Your article will be published under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license, just like every article on this blog. You are welcome to republish it on your own blog, though we ask that you wait until it has gone live on `tty1`.
