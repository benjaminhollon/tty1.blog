---
title: Get Started
subtitle: <code class="language-bash"><span class="token function">man</span> tty1.blog</code>
description: While for the most part, it doesn't matter where in tty1's archives you start, you should probably read these posts first.
---

While for the most part, it doesn't matter where in tty1's archives you start, you should probably read these posts first:

1. [Introduction](/articles/introduction/)
1. [Anatomy of a Command](/articles/anatomy-of-a-command/)
