<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml" data-font-family="monospace">
			<head>
				<title>"<xsl:value-of select="atom:feed/atom:title"/>" Atom Feed preview</title>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,shrink-to-fit=no" />
				<link href="/assets/css/readable.css" rel="stylesheet" type="text/css" />
				<link href="/assets/css/global.css" rel="stylesheet" type="text/css" />
				<link href="/assets/css/themes.css" rel="stylesheet" type="text/css" />
				<script src="/assets/js/themeSelector.js"></script>
			</head>
			<body onload="updateThemeSelector()">
				<header>
					<h1><xsl:value-of select="atom:feed/atom:title"/></h1>
					<p>Atom Feed preview</p>
				</header>
				<nav>
					<span><a href="/">Home</a></span>
					<span><a href="/get-started/">Get Started</a></span>
					<span><a href="/articles/">Articles</a></span>
					<span><a href="/subscribe/">Subscribe</a></span>
				</nav>
				<main>
					<p>To subscribe, just copy the URL of this page into a feed reader.</p>
					<p style="text-align: center">
						<a hreflang="en" target="_blank">
							<xsl:attribute name="href">
								<xsl:value-of select="atom:feed/atom:link/@href"/>
							</xsl:attribute>
							Visit Main Site &#x2192;
						</a>
					</p>
					<h2>Recent Posts</h2>
					<xsl:for-each select="atom:feed/atom:entry">
						<article>
							<h3 style="text-align: center">
								<a target="_blank">
									<xsl:attribute name="href">
										<xsl:value-of select="atom:link/@href"/>
									</xsl:attribute>
									<xsl:value-of select="atom:title"/>
								</a>
							</h3>
							<p style="text-align: center">
								Published:
								<time>
									<xsl:value-of select="atom:published" />
								</time>
							</p>
						</article>
					</xsl:for-each>
				</main>
				<ul id="themeSelector"></ul>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
