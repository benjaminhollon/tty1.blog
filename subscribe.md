---
title: Subscribe
subtitle: <code class="language-bash"><span class="token builtin class-name">echo</span> <span class="token string">"https://tty1.blog/feed/"</span> <span class="token operator">&gt;&gt;</span> ~/.config/newsboat/urls</code>
description: tty1 has an Atom Feed for you to get updates. newsboat is our terminal client of choice!
---

To get updates from tty1, use an Atom Feed reader, subscribing to the link [https://tty1.blog/feed/](https://tty1.blog/feed/). You can also open the link to see a preview of the feed. For an explanation of what Atom is, check out [About Feeds](https://aboutfeeds.com).

For a good terminal client to check your feeds, I highly recommend [newsboat](https://newsboat.org), the client I use. The website has a comprehensive guide, but of course you can also run `man newsboat` to get information on using it.
