---

title: tty1.blog
subtitle: I'm afraid it's terminal
description: tty1 is a blog about the Linux terminal.

---

```bash
$ hostname
tty1.blog

$ cat about-$(hostname).toml
description = "tty1 is a blog about the Linux terminal."
author = "Benjamin Hollon"
rss_feed = "https://tty1.blog/feed/"
```
